# Assignment-FE

## Getting started

- Take clone of this repository.
- Move inside **assignment-fe**.

```
cd assignment-fe
```

- Install node modules.

```
npm install
```

- Run App

```
ng serve
```

## Sample Dataset

- Sample dataset is added inside _assets_
