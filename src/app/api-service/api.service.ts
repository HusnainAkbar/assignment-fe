import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  api: string;
  getEndpoint: string;
  uploadEndpoint: string;


  constructor(public http: HttpClient) {
    this.api = 'http://localhost:3000/';
    this.getEndpoint = 'invoices';
    this.uploadEndpoint = 'upload';
  }

  getFiles(): Observable<any> {
    return this.http.get(`${this.api}${this.getEndpoint}`);
  }

  uploadFile(form: FormData): Observable<any> {
    return this.http.post(`${this.api}${this.uploadEndpoint}`, form);
  }
}
