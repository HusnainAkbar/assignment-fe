import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api-service/api.service';
import { IError, IFile } from './file.interface';
import { FileService } from './file.service';

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.scss']
})
export class FileInputComponent {

  file: any;
  fileName: string;
  fileType: string;
  isSuccess: boolean;
  errors: IError[];

  constructor(private fileService: FileService, private apiService: ApiService) {
    this.fileName = '';
    this.fileType = 'csv';
    this.isSuccess = false;
    this.errors = [];
  }

  onFileSelected(event: Event): void {
    if (event?.target) {
      const target= event.target as HTMLInputElement;
      this.file = (target.files as FileList)[0];
      this.fileName = this.file.name;
      const fileType = this.fileService.checkFileType(this.fileName);
      if (fileType !== this.fileType) {
        this.errors.push({
          row: -1,
          error: 'File type must be csv.'
        })
        return;
      }
      this.fileReader(this.file);
    }
  }

  fileReader(file: File): void {
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = () => {
      let csvData = reader.result;
      let jsonData: IFile[] = this.fileService.csvJSON(csvData);
      jsonData.forEach((cell, i) => {
        if (!cell.internalInvoiceId || (!cell.invoiceAmount && cell.invoiceAmount !== 0) || cell.invoiceAmount <= -1 || !cell.dueDate || !this.fileService.isValidDate(cell.dueDate)) {
          this.errors.push({
            row: i + 1,
            error: this.fileService.getErrorType(
              !cell.internalInvoiceId ? 0 : (!cell.invoiceAmount && cell.invoiceAmount !== 0) || cell.invoiceAmount <= -1 ? 1 : !cell.dueDate || !this.fileService.isValidDate(cell.dueDate) ? 2 : -1
            )
          })
        }
      })
    };
  }

  uploadCSVFile(): void {
    if(this.errors.length) {
      alert(JSON.stringify(this.errors))
    } else {
      const formData:FormData = new FormData();
      formData.append('file', this.file);
      this.apiService.uploadFile(formData).subscribe(response => {
        alert(JSON.stringify('Upload Successful'))
        this.isSuccess = true;
      })
    }
  }

}
