export interface IFile {
    internalInvoiceId: string;
    invoiceAmount: number;
    dueDate: string;
}

export interface IError {
    row: number;
    error: string;
}