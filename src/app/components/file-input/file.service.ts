import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IError, IFile } from './file.interface';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  errorTypes: string[];
  
  constructor(public http: HttpClient) {
    this.errorTypes = [
      'Internal Invoice ID is required.',
      'Invoice Amount is required.',
      'Due Date is required and must be in MM/DD/YYYY format.',
    ]
  }

  checkFileType(name: string): string {
    const splits = name.split('.');
    return splits[splits.length - 1];
  }

  isValidDate(input: string): boolean {
    const date = new Date(input);
    return date instanceof Date && !isNaN(date.getTime());
  }

  getErrorType(index: number): string {
    return index > -1 ? this.errorTypes[index] : 'Unknown error please check the row.';
  }

  csvJSON(csvText: any): any {
    const lines = csvText.split("\n");
    const result = [];
    for (var i = 1; i < lines.length-1; i++) {
      var currentLine = lines[i].split(",");
      let lineObj: IFile = {
        internalInvoiceId: currentLine[0], // 0 index contains internal invoice id
        invoiceAmount: currentLine[1], // 1 index contains invoice amount
        dueDate: currentLine[2], // 2 index contains due date
      };
      result.push(lineObj);
    }
    return result;
 }
}
