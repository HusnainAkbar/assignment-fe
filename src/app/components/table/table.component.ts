import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api-service/api.service';
import { IInvoice } from './table.interface';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {

  @Input() invoices?: IInvoice[];

  headers: string[];
  constructor( private apiService: ApiService ) {
    this.headers = [
      'Internal Invoice ID',
      'Due Date',
      'Upload Date',
      'Amount',
      'Selling Price',
    ]
  }

  fetchData() {
    this.apiService.getFiles().subscribe(invoices => {
      this.invoices = invoices[0];
    })
  }

}
