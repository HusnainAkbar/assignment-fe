export interface IInvoice {
    id: string;
    internal_invoice_id: string;
    amount: number;
    due_date: string;
    upload_date?: string;
    selling_price?: number;
}