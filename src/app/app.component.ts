import { Component, OnInit } from '@angular/core';
import { ApiService } from './api-service/api.service';
import { IInvoice } from './components/table/table.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app-fe';
  invoices?: IInvoice[];

  constructor(private apiService: ApiService) { }
  
  ngOnInit() {
    this.apiService.getFiles().subscribe(invoices => {
      this.invoices = invoices[0];
    })
  }
}
